---
title: KDE Games
layout: page
permalink: /games/
pagination: 
  enabled: true
---

<h1> KDE Games </h1>

<br><br>
<ul>
{% for page in site.pages %}
    {% if page.layout == 'game-category' %}
        <li><a href="{{ page.permalink }}">{{page.title}}</a></li>
    {% endif %}
{% endfor %}
</ul>
