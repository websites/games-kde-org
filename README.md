# Source Code for the website games.kde.org

Made using KDE Jekyll Theme.

## Build Instructions

```bash
gem install bundler --user-install
bundle install --path vendor/bundle
bundle exec jekyll serve
```

## Run in Development

```bash
bundle exec jekyll serve
```

## Production Build

```bash
bundle exec jekyll build
```
