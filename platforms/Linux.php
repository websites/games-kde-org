<?php
  $out = 'We were unable to detect your Linux distribution. Please use the following list to select one: <br />';
  $out .= '<ul>';
  global $distros;
  foreach( $distros as $distro ) {
    $out .= '<li><a href="./get.php?platform='.$distro.'">'.$distro.'</a></li>';
  }
  $out .= '</ul>';
  return $out;
?>