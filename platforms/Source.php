<?php
$out = "<ul><li>You can compile KDE on all supported platforms. The process of compiling from sources is more complicated than installing distribution packages and is <strong>Not</strong> recommended for users.<br /></li>
<li><a href='http://techbase.kde.org'>KDE's Techbase</a> <a href='http://techbase.kde.org/index.php?title=Getting_Started'>teaches</a> how to build KDE.</li></ul>";
return $out;
