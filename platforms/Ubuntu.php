<ul><li>To install <b>KDE Games</b> on your <b>Kubuntu/Ubuntu</b> you should install<br />
<i>'kdegames'</i> package using either Synaptic or System Settings->Add remove packages.
<br /><br /></li>
<li>Alternatively you can use apt: <i>sudo apt-get install kdegames</i></li></ul>
