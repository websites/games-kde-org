---
name: kmahjongg
title: KMahjongg
layout: game
category: board
version: 0.8
credits:
  year: 1998
  license: GPL-2.0-only
authors:
  - name: Mathias Mueller
    email: in5y158@public.uni-hamburg.de
    maintainer: true

  - name: Mauricio Piacentini
    email: mauricio@tabuleiro.com
    maintainer: true

  - name: Albert Astals Cid
    email: aacid@kde.org
    maintainer: true

  - name: David Black
    email: david.black@lutris.com
    maintainer: true

  - name: Michael Haertjens
    email: mhaertjens@modusoperandi.com
    maintainer: true    

contributors:
  - name: Raquel Ravanini
    email: raquel@tabuleiro.com

  - name: Richard Lohman
    email: richardjlohman@yahoo.com

  - name: Osvaldo Stark
    email: starko@dnet.it

  - name: Benjamin Meyer
    email: ben+kmahjongg@meyerhome.net

  - name: Eugene Trounev
    email: irs_me@hotmail.com

description: |
  In KMahjongg the tiles are scrambled and staked on top of each other to resemble a certain shape. The player then is expected to remove all the tiles off the game board by locating each tile’s matching pair.

howto: |
  **Objective:** *Remove all the tiles off the game board by locating each tile’s matching pair as fast as possible.*
  
  **KMahjongg** will load a default **layout** automaticly one once you start the game and you can start playing right away.
  
  **Note:** *The game timer will not start until you make the first move.*

  You should carefully study the stack of tiles lied out on the game board to find two tiles matching exactly (a matching pair). When you have found such a **pair** use your mouse to select these.

  **Note**: *Bear in mind that you can only select the tiles which have either of their vertical sides open.*

  Once you have selected the right **pair** of tiles they will vanish off the game board, thus opening the tiles previously located underneath.

  Find as many matches as possible to remove all the tiles from the game board.
---
