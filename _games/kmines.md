---
title: KMines
name: kmines
layout: game
category: logic
version: 3.0
credits:
  year: 1996
  license: GPL-2.0-only
authors:
  - name: Nicolas Hadacek
    email: hadacek@kde.org
    maintainer: true

  - name: Mauricio Piacentini
    email: mauricio@tabuleiro.com
    maintainer: true

  - name: Dmitry Suzdalev
    email: dimsuz@gmail.com
    maintainer: true
    
contributors:
  - name: Andreas Zehender 
    email:

  - name: Mikhail Kourinny 
    email:

  - name: Thomas Capricelli 
    email:

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Anton Brondz
    email: dinolinux@gmail.com

description: 
  KMines is the classic Minesweeper game. The idea is to uncover all the squares without blowing up any mines. When a mine is blown up, the game is over.

howto: |
  **Objective:** *Locate all the mines hidden on the minefield.*
  
  To play **KMines**, you need to use the mouse and its three buttons to **uncover** or to flag the squares (with two-buttoned mice, clicking the **Middle Mouse Button** is generally achieved by simultaneously pressing the **Left Mouse Button** and the **Right Mouse Button** buttons).

  Clicking the **Left Mouse Button** on your mouse will **uncover** a square. If there is a **mine** there, it will detonate, and the game will be over. If there is not a **mine** under the square, the square is cleared and if there are no other **mines** nearby, the square will disappear including any nearby squares without **mines**. If there are **mines** nearby, a number will appear showing how many neighboring squares contain **mines**. For each square (*excluding edge and corner squares*), there are eight neighboring squares.

  **Note**: ***Left Mouse Button clicking*** *a flagged square is safe and does nothing.*

  The **Right Mouse Button** will mark a square as containing a **mine** (by placing a red flag on it). Clicking the button twice will set it as being uncertain (by placing a question mark on it). The uncertain tag can be useful when you are puzzled about the positions of **mines**. **The Middle Mouse Button** will clear the surrounding squares if the right number of squares is already flagged. It is very useful since it is much quicker than uncovering all individual squares. Make sure yout flags are correctly placed, if not, you might detonate a **mine**.
---
