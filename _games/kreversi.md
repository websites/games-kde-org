---
name: kreversi
title: KReversi
layout: game
category: board
version: 2.0
credits:
  year: 1997
  license: GPL-2.0-only
authors:
  - name: Mario Weilguni
    email: mweilguni@sime.com
    maintainer: true

  - name: Inge Wallin
    email: inge@lysator.liu.se
    maintainer: true

  - name: Dmitry Suzdalev
    email: dimsuz@gmail.com
    maintainer: true 

contributors:
  - name:  Simon Hürlimann

  - name: Mats Luthman

  - name: Arne Klaassen

  - name: Mauricio Piacentini

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com    

description: |
  KReversi is a simple one player strategy game played against the computer. If a player's piece is captured by an opposing player, that piece is turned over to reveal the color of that player. A winner is declared when one player has more pieces of his own color on the board and there are no more possible moves. 

howto: |
  **Objective:** *Get control over the majority of squares on the board.*
  
  **KReversi** loads directly into the game play mode. As a first player you have the right to move first.
  
  **Note:** *By default the human player always has a right to the first move.*

  Now, imagine yourself that with each move your **stone** has to hop (jump) over the enemy's one (or ones, as there might be more then one stone). Yet, your **stone** does not relocate after the move, instead a new **stone** is placed on the board onto where your **stone** would have appeared if it indeed could jump. Once the move is done, all the enemy **stones** that you jump over, become yours and change their color respectively.

  Every single move you do in KReversi has to be done in this manner. And you can jump your stones vertically, horizontally and diagonally. But, in case there is nowhere to jump, your turn will automatically skip.

  **Note**: *The same rule applies to your enemy. If there is no possibility to make a move the turn is skipped.*

  The idea here is to fill the board with the **stones** of your own color. The player who manages to conquer the most of the game board is announced the winner.
---
