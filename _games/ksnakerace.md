---
name: ksnakerace
title: KSnake Race
layout: game
category: arcade
version: 1.0
authors:
  - name: Michel Filippi
  - name: Robert Williams
  - name: Andrew Chant

description: |
  *KSnake Race* is a fast action game where you steer a snake
  which has to eat food. While eating the snake grows. But
  once a player collides with the other snake or the wall the
  game is lost. This becomes of course more and more difficult
  the longer the snakes grow.
---
