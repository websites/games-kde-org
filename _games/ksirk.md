---
title: KsirK
name: ksirk
layout: game
category: strategy
version: 4.6
credits:
  year: 1995-2008
  license: GPL-2.0-only
authors:
  - name: Gaël de Chalendar
    email: kleag@free.fr
    maintainer: true
    
contributors:
  - name: Eugene Trounev
    email: irs_me@hotmail.com

description: 
  KsirK is a computerized version of a well known strategy board game. KsirK is a multi-player network-playable game with an AI. The goal of the game is simply to conquer the World... It is done by attacking your neighbours with your armies.

howto: |
  When you start **KsirK**, press **New Game** to start a new game you will see a dialog in which you need to choose the skin (map and countries) to use, how much players there will be and if they are played by the computers. You will also be able to start a network game if needed. Finally, you can choose the type of game: conquer the world or reach a specific goal like e.g. conquer Africa and Asia continents. 

  Then you will choose the nationality and name of local players. At the beginning of the game, countries are distributed to all the players. Each country contain one army (represented by an infantryman in the default skin) at this moment. Each player has some armies to distribute to his countries. Note that five armies are represented by a cavalryman and 10 by a cannon. 

  On each turn, each player can attack his neighbours, eventually conquering one or more countries. At the end of each turn, some bonus armies are distributed to the players in function of the number of countries they own. 
---
