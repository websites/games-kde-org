---
title: KBreakout
name: kbreakout
layout: game
category: arcade
version: 1.0.1
credits:
  year: 2008
  license: GPL-2.0-only
authors:
  - name: Fela Winkelmolen
    email: fela.kde@gmail.com
    maintainer: true

  - name: Eugene Trounev
    email: eugene.trounev@gmail.com
    maintainer: true

description: |
  The object of the game is to destroy as many bricks as possible without losing the ball. 

howto: |
  The player is presented with a game field containing a number of bricks. A ball travels across the screen, bouncing off the top and side walls. When a brick is hit, the ball bounces away and the brick is destroyed. The player loses a life when the ball touches the bottom of the field. To prevent this from happening, the player has to use a movable bar to bounce the ball upward, situated at the bottom of the window. The purpose of the game is to progress through the levels destroying all bricks present in each, and trying to get a highscore. 
---
