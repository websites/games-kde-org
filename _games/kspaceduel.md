---
title: KSpaceduel
name: kspaceduel
layout: game
category: arcade
version: 2.0
credits:
  year: 1998
  license: GPL-2.0-only
authors:
  - name: Andreas Zehender
    email: az@azweb.de
    maintainer: true

  - name: Branan Riley
    email: branan@gmail.com
    maintainer: true
    
description: |
  In KSpaceduel each of two possible players controls a satellite spaceship orbiting the sun. As the game progresses players have to eliminate opponent’s spacecraft. 

howto: |
  **Objective:** *Destroy the opponent's **satellite**, while keeping the own one in tact.*
  
  **KSpaceduel** loads directly into the game mode; however, the action does not start until either player makes the first move.

  **Note**: *If the opponent’s **satellite** is controlled by the built in artificial intelligence, game action does not start until the human player makes the first move.*

  In **KSpaceduel** you control the ship’s rotation, acceleration, and weaponry. Your **satellite** is constantly drawn towards the sun by gravity. You have to adjust the rotation and acceleration of your ship to make it stay on course.

  **Note**: *Approaching the sun too close causes the satellite to explode.*

  In the same time you have to monitor the opponent’s movement and use your weapons consisting of bullets and mines, to destroy the enemy spacecraft.

  **Note**: *Your own weapons are as harmful to your satellite as they are to the enemy one.*
---
