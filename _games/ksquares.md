---
title: KSquares
name: ksquares
layout: game
category: logic
version: 0.6
credits:
  year: 2006
  license: GPL-2.0-only
authors:
  - name: Matt Williams
    email: matt@milliams.com
    maintainer: true

contributors:
  - name: Fela Winkelmolen
    email:

description: 
  KSquares is a fun and exciting game for desktop environment. The game is modeled after the well known pen and paper based game of Dots and Boxes.

howto: |
  **Objective:** *Complete more squares than any of your opponents.*
  
  At the start of the game, the game board is empty. There is simply a grid of dots showing where lines can be drawn.

  **Note**: *Depending on the game difficulty chosen, the game field may already contain lines at the beginning of the game round.*

  Each player takes it in turns to draw a line between two adjacent dots on the board. By hovering the mouse over the game board, a yellow indicator line will show you where your line will be placed when you click. Once you have decided where you want to draw your line, click - and the line will be drawn. If by drawing a line, you completed a square then this square now belongs to you and earns you a point. Each time you complete a square, you may draw another line. If your line did not complete a square then the next player(s) take their turn. The game will continue until every square on the board owned by one of the players.

  To start a new game with custom settings (number of players, player names, board size) click on the "New" toolbar item or go to **File->New**. To start a new game with the same settings as the current game, click on **Reset** button on the toolbar. 
---
