---
title: KLines
name: klines
layout: game
category: arcade
version: 1.5
credits:
  year: 2000
  license: GPL-2.0-only
authors:
  - name: Roman Merzlyakov
    email: roman@sbrf.barrt.ru
    maintainer: true

  - name: Roman Razilov
    email: Roman.Razilov@gmx.de
    maintainer: true

  - name: Dmitry Suzdalev
    email: dimsuz@gmail.com
    maintainer: true
    
contributors:
  - name: Eugene Trounev
    email: eugene.trounev@gmail.com

description: |
  KLines is a simple but highly addictive, one player game for KDE. KLines has been inspired by well known game of Color Lines, written by Olga Demina, Igor Ivkin and Gennady Denisov back in 1992. You can also play an online demo version of this game.

howto: |
  **Objective:** *Move the colored balls around the game board, gathering them into the lines of the same color by five.*
  
  When **Klines** starts you are presented with the game board split into 81 squares (the game board is 9x9 squares), and right away three new colored balls appear on the board. Use your mouse to move the balls from cell to cell to group them into the lines of the **same color**. However, after each of your moves computer drops three more balls onto the board. To avoid feeling up the board you should gather the balls into **lines of 5 or more balls**. When such line is complete, balls are removed from the field and your **score** grows.

  **Note**: The new balls will not be added to the field after a line removal. Instead you will be **rewarded** with yet another move before a new triplet of balls is added.

  The increase in **score** depends solely on the amount of the erased balls.

  **Note**: *If you are playing with ‘Show Next’ feature enabled the increase in score is less then if ‘Show Next’ feature is disabled.*

  **KLines** can not be won, and is played against the **highscore** exclusively. The game ends once the whole game board becomes filled up with balls.
---
