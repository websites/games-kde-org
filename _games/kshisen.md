---
name: kshisen
title: KShisen
layout: game
category: board
version: 1.8.2
credits:
  year: 1997
  license: GPL-2.0-only
authors:
  - name: Dave Corrie
    email: kde@davecorrie.com
    maintainer: true

  - name: Mario Weilguni
    email: mweilguni@sime.com
    maintainer: true

contributors:
  - name: Mauricio Piacentini
    email: mauricio@tabuleiro.com

  - name: Jason Lane
    email: jglane@btopenworld.com

  - name: Frederik Schwarzer
    email: schwarzer@kde.org

description: |
  KShisen is a solitaire-like game played using the standard set of Mahjong tiles. Unlike Mahjong however, KShisen has only one layer of scrambled tiles.

howto: |
  **Objective:** *Remove all the tiles from the game board within the shortest time possible.*
  
  **KShisen** will load a default layout automatically once you start the game and you can start playing right away.
  
  You should carefully study the tiles lied out on the game board and find two tiles matching exactly. When you have found such a pair use your mouse to select these.

  Once you select a right pair of tiles they will vanish from the game board. However, even if the tiles you select appear as open they will only be removed from the board if they can be connected with a line having no more than two bents and not crossing any other tiles (the line can not be diagonal).

  Find as many matches as possible to remove all the tiles from the game board.
---
