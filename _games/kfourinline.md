---
name: kfourinline
title: KFourInLine
layout: game
category: board
version: 1.40
credits:
  year: 1995
  license: GPL-2.0-only
authors:
  - name: Martin Heni
    email: kde@heni-online.de
    maintainer: true

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com
    maintainer: true

  - name: Eugene Trounev
    email: eugene.trounev@gmail.com
    maintainer: true

  - name: Benjamin Meyer
    maintainer: true

contributors:
  - name: Anton Brondz
    email: dinolinux@gmail.com

description: |
  KFourInLine is a board game for two players based on the Connect-Four game. The players try to build up a row of four pieces using different strategies. 

howto: |
  When **KFourInLine** loads, you can select the difficulty or start a two player game. If you like, you can also pick your color and select which color starts the game.
  
  Each player is represented by a color (for example yellow or white for player one and red for player two).
  
  The goal of the game is to get four connected pieces of your color into any row, column or diagonal.

  The players move in turns. Each turn the player can place one of his or her pieces into any of the seven columns where each piece will fall to the lowest possible free place.
  **Note**: *Bear in mind that you can only select the tiles which have either of their vertical sides open.*

  After one player's move is done the second player can make his or her input. This is repeated until the game is over, which is when one of the players has four pieces in a row, column or diagonal or no more moves are possible because the board is filled.

  A player who first manages to get four pieces in a row, column or diagonal wins the game. If no more moves can be made but no player has won, the game ends drawn.
---
