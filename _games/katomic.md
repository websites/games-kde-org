---
title: KAtomic
name: katomic
layout: game
category: logic
version: 3.0
credits:
  year: 1998
  license: GPL-2.0-only
authors:
  - name: Andreas Wuest
    email: AndreasWuest@gmx.de
    maintainer: true

  - name: Stephan Kulow
    email: coolo@kde.org
    maintainer: true

  - name: Cristian Tibirna
    email: tibirna@kde.org
    maintainer: true

  - name: Dmitry Suzdalev
    email: dimsuz@gmail.com
    maintainer: true
    
contributors:
  - name:  Carsten Pfeiffer
    email: pfeiffer@kde.org

  - name: Kai Jung
    email: jung@fh-fresenius.de

  - name: Danny Allen
    email: danny@dannyallen.co.uk

  - name: Johann Ollivier Lapeyre 
    email: johann.ollivierlapeyre@gmail.com

  - name: Eugene Trounev
    email: eugene.trounev@gmail.com    

description: 
  Katomic is both fun and educational game built around molecular geometry. It employs simplistic two-dimensional looks at different chemical elements. You can also play an online demo version of this game.

howto: |
  **Objective:** *Reassemble molecules from compound atoms by sliding them around.*
  
  Katomic will load level one once you start the game and you can begin playing right away.

  To see how the molecule you are supposed to make looks like, take a look at the upper right corner. To play - click on an atom. You will see green arrows pointing in the directions where atom can move. To move the atom, click on the desired arrow. When an atom starts moving, it will not stop until it hits another atom or a wall, so make sure you think before you do your next move.

  You can assemble your molecule wherever you like on the game board, but some places are easier to access than others. When the molecule is assembled, you can move to the next level.
---
