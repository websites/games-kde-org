---
title: KTuberling
name: ktuberling
layout: game
category: toy
version: 0.6.0
credits:
  year: 1999
  license: GPL-2.0-only
authors:
  - name: Éric Bischoff 
    maintainer: true

  - name: John Calhoun
    maintainer: true

  - name: Albert Astals Cid
    email: aacid@kde.org
    maintainer: true    
    
contributors:
  - name: Agnieszka Czajkowska
    email: agnieszka@imagegalaxy.de

  - name: Bas Willems
    email: cybersurfer@euronet.nl

  - name: Roger Larsson
    email: roger.larsson@norran.net

  - name: Dolores Almansa
    email: dolores.almansa@corazondemaria.org

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Michał Bartecki
    email: mkbart@gmail.com


description: 
  KTuberling a simple constructor game suitable for children and adults alike. The idea of the game is based around a once popular doll making concept.

howto: |
  **KTuberling** is a game intended for small children. Of course, it may be suitable for adults who have remained young at heart.

  It is a **"potato editor"**. That means that you can drag and drop eyes, mouths, mustache, and other parts of face and goodies onto a potato-like guy. Similarly, you have other playgrounds with different themes.

  There is no winner for the game. The only purpose is to make the funniest faces you can.

  **KTuberling** can also "speak". It will spell out the name of the objects you drag and drop. It will "speak" in a language that you can chose. You can even use it to learn a bit of vocabulary in foreign languages.
---
