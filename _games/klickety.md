---
title: Klickety
name: klickety
layout: game
category: arcade
version: 2.0
credits:
  year: 1997
  license: GPL-2.0-only
authors:
  - name: Nicolas Hadacek
    email: hadacek@kde.org
    maintainer: true

  - name: Ni Hui
    email: shuizhuyuanluo@126.com
    maintainer: true

  - name: Henrique Pinto
    email: henrique.pinto@kdemail.net
    maintainer: true

  - name: Marcus Kreutzberger
    email: kreutzbe@informatik.mu-luebeck.de
    maintainer: true

contributors:
  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

description: |
  Klickety is a simple, yet challenging color matching game modeled after once famous game of SameGame. The idea behind Klickety is to completely clear the game board filled with the multicolored marbles. Klickety also offers a SameGame mode called KSame. 

howto: |
  **Objective:** *Clear all the multicolored marbles off the game board.*
  
  Once the game starts you will be presented with a board filled with multicolored **marbles**. You can erase some of them as long as they are of the same color and are aligned either vertically or horizontally. If there are pieces of other color located over the erased **marbles**, they will drop down. In the same manner, all the **marbles** located on the right from the erased ones will shift to the left. You are expected to clear as much of the game board area as possible.

  **Note**: Remember, you will be penalized for each remaining **marble**.
---
