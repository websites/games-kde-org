---
title: Kolf
name: kolf
layout: game
category: arcade
version: 1.10
credits:
  year: 2004
  license: GPL-2.0-only
authors:
  - name: Jason Katz-Brown
    email: jasonkb@mit.edu
    maintainer: true

  - name: Niklas Knutsson 
    email:
    maintainer: true

  - name: Rik Hemsley 
    email:
    maintainer: true

  - name: Ryan Cumming
    email:
    maintainer: true

  - name: Daniel Matza-Brown 
    email:
    maintainer: true

  - name: Timo A. Hummel
    email: timo.hummel@gmx.net
    maintainer: true

contributors:
  - name: Rob Renaud 
    email:

  - name: Aaron Seigo
    email: aseigo@kde.org

  - name: Eugene Trounev
    email: irs_me@hotmail.com

description: |
  Kolf is a miniature golf game with 2d top-down view. Courses are dynamic, and up to 10 people can play at once in competition. Kolf comes equipped with a variety of playgrounds and tutorial courses.

howto: |
---
