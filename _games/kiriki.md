---
title: Kiriki
name: kiriki
layout: game
category: dice
version: 0.2
credits:
  year: 2005
  license: GPL-2.0-only
authors:
  - name: Albert Astals Cid
    email: aacid@kde.org
    maintainer: true

contributors:
  - name: Eugene Trounev
    email: irs_me@hotmail.com

description: |
  Kiriki is an addictive and fun dice game for **KDE**, designed to be played by as many as six players. Participants have to collect points by rolling five dice for up to three times per single turn. 

howto: |
  **Objective:** *Earn more score points then either of the opponents.*
  
  Once Kiriki starts, it right away gives out the first combination for the **player one**. If you are the **player one**, then it is now your turn, otherwise you will have to wait until all other players make their moves.

  **Note**: By default, unless the game settings have been changed, and in single player mode, the first player is controlled by human (you), while all other five players are controlled by Artificial Intelligence.

  Once it is your turn, the first thing you should do is examine the current roll. Each of the five dice on the left hand side of the game screen has a specific value. Check the sequence to see if these values form any of the **score patterns**.

  Keep in mind that the combination can match more then one **score pattern**. In this case you will have to determine which **score pattern** will result in greater points for you.

  In case the current sequence matches no **score pattern**, or matches a **score pattern** you have already used up, you have two more rolls to fix the situation. To use your second (third) roll select the dice you are not satisfied with by clicking on them with your mouse and then clicking **Roll** button located right under the dice screen. This will cause the dice in question to reroll, thus changing their values.

  After you have done the third reroll you must choose the **score pattern** which feats the dice sequence on hand. If none of the **score patterns** feet the sequence - you can either use **Chance score pattern**, or just select any **score pattern** on your scoreboard.

  **Note**: *If you select score pattern that does not feet current dice sequence, the score pattern you select will become void and of zero value.*

  As soon as you choose the **score pattern** its value updates to reflect the dice sequence you currently have.

  Right after that your turn ends and the next participant’s turn begins. This sequence repeats some thirteen times and then the game ends, the scores are calculated and the high scores are awarded.
---
