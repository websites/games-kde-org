---
title: KPat
name: kpat
layout: game
category: card
version: 3.5
credits:
  year: 1999
  license: GPL-2.0-only
authors:
  - name: Paul Olav Tvete
    email: paul@troll.no
    maintainer: true

  - name: Mario Weilguni
    email: mweilguni@kde.org
    maintainer: true

  - name: Matthias Ettrich
    email: ettrich@kde.org
    maintainer: true

  - name: Rodolfo Borges
    email: barrett@9hells.org
    maintainer: true

  - name: Peter H. Ruegg
    email: kpat@incense.org
    maintainer: true

  - name: Michael Koch
    email: koch@kde.org
    maintainer: true

  - name: Marcus Meissner
    email: mm@caldera.de
    maintainer: true

  - name: Dr. Tom
    maintainer: true

  - name: Stephan Kulow
    email: coolo@kde.org
    maintainer: true

  - name: Erik Sigra
    email: sigra@home.se
    maintainer: true

  - name: Josh Metzler
    email: joshdeb@metzlers.org
    maintainer: true

  - name: Maren Pakura
    email: maren@kde.org
    maintainer: true

  - name: Inge Wallin
    email: inge@lysator.liu.se
    maintainer: true

  - name: Simon Hürlimann
    email: simon.huerlimann@huerlisi.ch
    maintainer: true  
    
contributors:
  - name: Johann Ollivier Lapeyre 
    email: johann.ollivierlapeyre@gmail.com

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Parker Coates
    email: parker.coates@kdemail.net

description: |
  KPat (aka KPatience) is a relaxing card sorting game. To win the game a player has to arrange a single deck of cards in certain order amongst each other. 

howto: |
  **Objective:** *Rearrange the cards by suite within shortest time possible.*
  
  Essentially there are two types of **solitaire** games – the ones that let you arrange the cards **by same color** and the ones that let you arrange the cards by **alternating color**. You can easily determine which game you are trying out using trial-and-error method. Once you know which one you are dealing with, rest is easy; keep arranging and rearranging the cards, collecting the **matching** ones.

  From the beginning however, pay attention to your card stacks. Do not rush to move individual cards around, see if it might be more useful elsewhere.

  For additional help you can also take quick picks onto the statusbar. For the most of the games there is a built in **solver** which informs you whether the game you are currently playing can be complete or not. If you see that **solver** could not complete the current game, chances are you will not be able to do so as well. At this point you can either **restart** the current game, or start a completely **new game**.

  **Note**: *Playing solitaire games helps you to greatly improve your attention span and enhance the ability to concentrate.*
---
