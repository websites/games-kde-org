---
layout: page
title: Download
css-include: /css/download.css
sorted: 3

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        KDE Games are already available on the majority of Linux distributions.
        You can install them from the Discover or GNOME Software.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        You can find KDE Games latest stable release as a
        <a href="https://download.kde.org/stable/release-service/">tarballs</a>.

        If you want to build KDE Games from source, we recommend checking out our
        <a href="/get-involved">Getting Involved</a> page, which contains links to
        a full guide how to compile KDE Games yourself.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        KDE Games git repository can be viewed
        <a href="https://invent.kde.org/games">using KDE's GitLab instance</a>.

        To clone KDE Games, use <code>git clone
        https://invent.kde.org/games/&lt;game-identifier&gt;.git</code>. For detailed instructions on how
        to build KDE Games from source, check the <a href="/get-involved">Getting
        Involved page</a>
---

# Download

<table class="distribution-table">
  {% for source in page.sources %}
    <tr class="title-row">
      <td rowspan="2" width="100">
        <img src="{{ source.icon }}" alt="{{ source.name }}">
      </td>
      <th>{{ source.name }}</th>
    </tr>
    <tr>
      <td>{{ source.description }}</td>
    </tr>
  {% endfor %}
</table>
