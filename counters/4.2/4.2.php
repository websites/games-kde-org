<?php
	$Langs['En']['Far'][]      = '%s days until the impossible';
	$Langs['En']['Far'][]      = 'Shift into a new gear with KDE 4.3 in %s days';
	$Langs['En']['Far'][]      = '4.3: A clear upgrade path; %s days remaining';
	$Langs['En']['Far'][]      = 'Discover beauty with 4.3, due out in %s days';
	$Langs['En']['Far'][]      = 'Next generation technology coming to you in %s days';
	$Langs['En']['Tomorrow'][] = '4.3 will be released tomorrow!';
	$Langs['En']['Today'][]    = '4.3 is going to be released today!';
	$Langs['En']['Out'][]      = 'KDE 4.3 is released!';

	$Langs['De']['Far'][]      = '%s Tage noch bis zur VerÃ¶ffentlichung';
	$Langs['De']['Far'][]      = 'Schalte einen Gang hoch mit KDE 4.3 - in %s Tagen';
	$Langs['De']['Far'][]      = '4.3: Ein direkter Weg. Nur noch %s Tage zu warten';
	$Langs['De']['Tomorrow'][] = 'Morgen wird 4.3 verÃ¶ffentlicht!';
	$Langs['De']['Today'][]    = 'Seit heute ist KDE 4.3 verfÃ¼gbar';
	$Langs['De']['Out'][]      = 'KDE 4.3 ist da!';

	$Langs['Fr']['Far'][]      = "%s jours avant l\'impossible";
	$Langs['Fr']['Far'][]      = "Passez Ã  la vitesse supÃ©rieure avec KDE 4.3 dans %s jours";
	$Langs['Fr']['Far'][]      = "KDE 4.3: plus qu\'une mise Ã  jour. %s jours restants";
	$Langs['Fr']['Far'][]      = "DÃ©couvrez la beautÃ© de KDE 4.3. %s jours restants";
	$Langs['Fr']['Far'][]      = "La prochaine gÃ©nÃ©ration arrive dans %s jours";
	$Langs['Fr']['Tomorrow'][] = "KDE 4.3 sera disponible demain!";
	$Langs['Fr']['Today'][]    = "KDE 4.3 sera disponible dans la journÃ©e!";
	$Langs['Fr']['Out'][]      = "KDE 4.3 est disponible!";

	$Url        = 'http://games.kde.org/counters/4.2/';
	$Released   = False;
	$Now        = time();
	$Release    = mktime( 0, 0, 0, 7, 28, 2009 );
	$Remaining  = $Release - $Now;
	$Day        = 86400;
	$Week       = $Day * 7;
    $Background = '4.2-0';

	header('Cache-Control: no-cache');

	if ( !$Released ) {

		$RemainingDays = (int) Ceil( $Remaining / $Day );

		if ( $RemainingDays > 1 ) {
			$Text = sprintf( GetLang('Far'), $RemainingDays );
		} else if ( $RemainingDays === 1 ) {
			$Text = GetLang('Tomorrow');
		} else if ( $RemainingDays === 0 ) {
			$Text = GetLang('Today');
		}

        Switch ( $RemainingDays ) {
            Case 3:
            Case 2:
            Case 1:
                $Background = '4.2-3';
            Break;
            Case 0:
                $Background = '4.2-0';
            Break;
        }
	} else {
		$Released = True;
		$Text     = GetLang('Out');
	}
	print JS(HTML($Text, $Background));

	function GetLang($Name) {
		$Lang = isset( $_GET['Lang'] ) ? $_GET['Lang'] : 'En';
		$Key  = array_rand($GLOBALS['Langs'][$Lang][$Name]);
		return $GLOBALS['Langs'][$Lang][$Name][$Key];
	}

	function HTML($Text, $Background = '4.2') {
		return '<div style="background-image: url('.$GLOBALS['Url'].'/'.$Background.'.png);background-repeat:no-repeat;width: 478px;height: 20px;text-align: center;padding-top: 66px;font: bold 12px Sans;color: #fff;">'.$Text.'</div>';
	}

	function JS($Text) {
		return 'document.write(\''.$Text.'\')';
	}
?>
