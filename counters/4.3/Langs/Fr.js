Langs['4.3/Counter']['Banner'] = Array('','Configuration simple et intuitive','Gestion des fichiers meilleure que jamais','Visionnage aisé des photos et images','Toute la puissance du Web au bout de vos doigts',"Lanceur d'applications et de recherches amélioré",'Solutions de communication fantastiques','Une expérience du bureau jamais vue','Plus de 60 000 mises à jour','Plus de 2 000 nouvelles fonctionnalités');
Langs['4.3/Counter']['Square'] = Array('','Configurabilité','Gestion des fichiers','Visionnage de photos','Surf sur le Web2.0','Menus améliorés','Communication','Meilleur bureau','60 000 mises à jour','2 000 nouvelles fonctionnalités');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'La communauté KDE publiera KDE 4.3 dans %s jours';
	Langs['4.3/Counter']['Square'][0]  = '%s jours restants';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = "KDE 4.3 sera publié aujourd'hui";
	Langs['4.3/Counter']['Square'][0]  = 'Jour de la publication';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 est publié';
	Langs['4.3/Counter']['Square'][0]  = '4.3 est publié';
}