Langs['4.3/Counter']['Banner'] = Array('','Basit ve sezgisel yapılandırma','Her zamankinden daha iyi dosya yönetimi','Fotoğraf ve resim görüntüleme kolaylaştırıldı',"Web'in gücü parmaklarınızın ucunda",'Geliştirilmiş arama ve uygulama çalıştırıcı','İnanılmaz iletişim çözümleri','Daha önce benzeri görülmemiş bir masaüstü deneyimi',"60.000'den fazla güncelleme","2.000'den fazla yeni özellik");
Langs['4.3/Counter']['Square'] = Array('','Yapılandırılabilirlik','Dosya yönetimi','Resim Görüntüleme','Web2.0 Gezinme','Gelişmiş Menüler','İletişim','Daha İyi Masaüstü','60.000 güncelleme','2.000 yeni özellik');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = "KDE ekibi %s gün sonra KDE 4.3'ü çıkaracak";
	Langs['4.3/Counter']['Square'][0]  = '%s gün kaldı';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 bugün çıkıyor';
	Langs['4.3/Counter']['Square'][0]  = 'Çıkış Günü';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 çıktı';
	Langs['4.3/Counter']['Square'][0]  = '4.3 çıktı';
}