Langs['4.3/Counter']['Banner'] = Array('',
,'إعداد سهل وبسيط'
,'إدارة الملفات أفضل من أي وقت مضى'
,'عرض الصور بسهولة'
,'قوة الانترنت في متناول يدك'
,'منفذ التطبيقات وبحث محسنين'
,'حلول رائعة للإتصالات'
,'تجربة سطح المكتب أفضل من أي وقت مضى'
,'أكثر من 60,000 تحديث'
'أكثر من 2,000 ميزة جديدة'
);
Langs['4.3/Counter']['Square'] = Array('',
,'التخصيص'
,'إدارة الملفات'
,'عرض الصور'
,'تصفح الويب2.0'
,'قوائم محسنة'
,'التواصل'
,'أفضل سطح مكتب'
,'60,000 تحديث'
'2,000 ميزة جديدة'
);
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'مجتمع كدي سيصدر كدي 4.3 خلال %s أيام';
	Langs['4.3/Counter']['Square'][0]  = '%s أيام متبقية';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'كدي 4.3 سيصدر اليوم';
	Langs['4.3/Counter']['Square'][0]  = 'يوم الصدور';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'صدر كدي 4.3';
	Langs['4.3/Counter']['Square'][0]  = '4.3 صدر';
}
