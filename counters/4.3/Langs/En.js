Langs['4.3/Counter']['Banner'] = Array('','Simple and intuitive configuration','Better than ever file management','Photo and Image viewing made easy','Power of the Web at your fingertips','Enhanced search and application launcher','Fantastic communication solutions','Desktop experience like never before','More than 60,000 updates','More than 2,000 all new features');
Langs['4.3/Counter']['Square'] = Array('','Configurability','File management','Picture Viewing','Web2.0 Browsing','Enhanced Menus','Communications','Better Desktop','60,000 updates','2,000 new features');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE community releases KDE 4.3 in %s days';
	Langs['4.3/Counter']['Square'][0]  = '%s days left';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 will be released today';
	Langs['4.3/Counter']['Square'][0]  = 'Release Day';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 is released';
	Langs['4.3/Counter']['Square'][0]  = '4.3 is Released';
}