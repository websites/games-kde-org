Langs['4.3/Counter']['Banner'] = Array('','Configuração simples e intuitiva','Gerenciamento de arquivos melhor que nunca','Visualização de fotos e imagens mais fácil','O poder da Web nos seus dedos','Lançador de aplicativos e busca melhorada','Soluções de comunicação fantasticas','Experiência do Desktop como nunca antes vista','Mais de 60.000 atualizações','Mais de 2.000 novos recursos');
Langs['4.3/Counter']['Square'] = Array('','Configurabilidade','Gerenciamento de Arquivos','Visualização de imagens','Navegação Web2.0','Menus melhorados','Comunicações','Melhor Desktop','60.000 atualizações','2.000 novos recursos');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Comunidade KDE lançara o KDE 4.3 em %s dias';
	Langs['4.3/Counter']['Square'][0]  = 'Mais %s dias';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 vai ser lançado hoje';
	Langs['4.3/Counter']['Square'][0]  = 'Dia de lançamento';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 foi lançado';
	Langs['4.3/Counter']['Square'][0]  = '4.3 está ai';
}