Langs['4.3/Counter']['Banner'] = Array('','Einfaches und intuitives Einrichten','Dateiverwaltung ist besser denn je','Foto- und Bilderansicht einfach gemacht','Die Möglichkeiten des Internet einfach Nutzen','Verbesserte Suche und Programmstarter','Fantastische Kommunikationslösungen','Ein Desktop-Erlebnis wie nie zuvor','Mehr als 60.000 Aktualisierungen','Mehr als 2.000 brandneue Funktionen');
Langs['4.3/Counter']['Square'] = Array('','Einrichten','Dateiverwaltung','Bilder anschauen','Web 2.0-Browsen','Erweiterte Menüs','Kommunikation','Besserer Desktop','60.000 Aktualisierungen','2.000 neue Funktionen');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'Die KDE-Gemeinschaft veröffentlicht KDE 4.3 in %s Tagen';
	Langs['4.3/Counter']['Square'][0]  = 'Noch %s Tage';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner']['De'][0]  = 'KDE 4.3 wird heute freigegeben';
	Langs['4.3/Counter']['Square']['De'][0]  = 'Veröffentlichungstag';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner']['De'][0]  = 'KDE 4.3 ist veröffentlicht';
	Langs['4.3/Counter']['Square']['De'][0]  = '4.3 ist veröffentlicht';
}