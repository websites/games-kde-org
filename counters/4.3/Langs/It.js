Langs['4.3/Counter']['Banner'] = Array('','Configurazione semplice e intuitiva','Gestione dei file migliore di sempre','Visualizzazione di Foto e Immagini semplicissima','La Potenza del Web alla punta delle tue dita',"Soluzioni avanzate per la ricerca e l'avvio delle applicazioni",'Fantastiche soluzioni di comunicazione','Un esperienza del Desktop come mai prima','Più di 60.000 aggiornamenti','Più di 2.000 nuove funzioni');
Langs['4.3/Counter']['Square'] = Array('','Configurabilità','Gestione dei File','Visualizzazione delle Immagini','Navigazione Web2.0','Menù Avanzati','Comunicazioni','Il miglior Desktop','60.000 aggiornamenti','2.000 nuove funzioni');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'La comunità di KDE rilascerà KDE 4.3 fra %s giorni';
	Langs['4.3/Counter']['Square'][0]  = 'Mancano %s giorni';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 verrà rilasciato oggi';
	Langs['4.3/Counter']['Square'][0]  = 'Il Giorno del Rilascio';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 è stato rilasciato';
	Langs['4.3/Counter']['Square'][0]  = '4.3 è stato Rilasciato';
}