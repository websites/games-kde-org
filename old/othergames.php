<?php
  $page_title = "Editor's choice (I)";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

      <p>
      <font size="+1">I</font>n this section we review various KDE
      games from all over the world. We present a short description
      and critics of the reviewed game. Note that the opinion is
      the opinion of the reviewer and not an <em>official</em> KDE
      opinion.<br />
      If you know about a nice game not listed here
      and want it reviewed just send a mail 
      <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann</a> and/or
      <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a>.
      </p>
      <p>
      Index of further review pages:
      </p>
      <ul>
	<li>
	<a href="othergames2.php">Page II of Editor's choice</a>
	</li>
      </ul>


    <table align="center" border="0" width="90%"><tr><td>
    <?php
      // maybe a loop would be useful?
      // but this could create more problems - so just do it this way
      // NOTE: We put only 10 games here. Old games must be shifted
      //       to the next page(s), ala othergames2.html, etc to not
      //       make the pages to long
      //
      // NOTE: Please put NO beta games here. Games listed here should
      //       be playable and a recommendation for KDE.
      include ("othergames/knights.inc");
      include ("othergames/taxipilot.inc");
      include ("othergames/kwappen.inc");
      include ("othergames/quintalign.inc");
      include ("othergames/freeforce.inc");
      include ("othergames/kpooka.inc");
      include ("othergames/kpuzzle.inc");
      include ("othergames/kpatience.inc");
      include ("othergames/kpicframer.inc");
      include ("othergames/boson.inc");
    ?>

    </td></tr></table>


<?php
INCLUDE "nospam.inc";
?>

<hr width="570" size="5" align="left" noshade="noshade" />
<p>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a
 href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>
</p>

<?php
INCLUDE "footer.inc";
?>

		  
