<?php
  $page_title = "New KDE Games";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

<!-- MH 03-2003: Currently not used page -->

		  
		  <FONT SIZE="+1">T</FONT>his section contains information
      and proposals for new games. If you are interested in 
      writing a game you can either 
      <a href="mailto:martin@NO__SPAMheni-online.de">propose</a> it here
      or join one of the already existing games programming groups.
      <p>

  &nbsp;<p>
      <!-- *********************************************************** -->

    <TABLE align="center" border="0" width="90%"><TR><TD>
    
    <h2>Open Projects</h2>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <b>
    <FONT size="+1">
     <IMG SRC="images/newgame2.png" WIDTH=52 HEIGHT=27 BORDER=0 ALT="new game" ALIGN="left">
     &nbsp;
    Anima - Sprite Animation Editor
    </FONT>
    </b>
    <p>
    Anima is a Linux/KDE program designed to facilitate the creation of animated sprite characters
    for open source games. The resulting artwork used by Anima is primarily useful in two-dimensional
    games, such as side-scrollers, but can also be used in 3D games for special effects and other
    objects that do not require a complete polygonal mesh.
    <p>

    Anima supports a hierarchical approach to sequence construction. The artist can create individual
    frames of animation, and then sequence them in a variety of ways. An individual frame can be used
    in more than once sequence. In addition, each individual frame can be composed of multiple elements,
    including elements which can be parametrically substituted at runtime - so for example a character
    holding a sword can also wield a mace or a dagger, without having to completely re-animate the character.
<p>
     <table>

     <tr>
     <td valign=top>
				<b>Contact:</b>  
     </td>
     <td valign=top>
        <A HREF="mailto:talin@NO__SPAMacm.org">Talin</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
			<b>	Homepage:  </b>
     </td>
     <td valign=top>
        <A HREF="http://anima.sourceforge.net">http://anima.sourceforge.net</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Screenshots:</b>
     </td>
     <td valign=top>
        <A HREF="http://anima.sourceforge.net/images/screenshot_1.png">http://anima.sourceforge.net/images/screenshot_1.png</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Proposed:</b>
     </td>
     <td valign=top>
          29th September 2000
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          Early alpha stage<br>
          <br>
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>


		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <b>
    <FONT size="+1">
     <IMG SRC="images/newgame2.png" WIDTH=52 HEIGHT=27 BORDER=0 ALT="new game" ALIGN="left">
     &nbsp;
    Civilisation The Rise and Fall of Empires<
    </FONT>
    </b>
    <p>
Civ RnF (Civilisation The Rise and Fall of Empires abbriviated) is a freeware game currently being developed under
Linux. It is under the GNU General Public License which means that the source code is open. Anyone can look at it
modify it and redistribute it. Civ RnF bears much resemblance to the popular civilization games (civI, civII, CTP,
SMAC etc). In fact the aim of this project is to take civilization to the next level. 
<p>
Interesting to note:<br>
There are some classes that are ready and working and which other developers might be interested in using. 
For instance an iosmetric tiling engine.
<p>
Remark: <br>
Currently the program is Qt and not strictly KDE. But maybe someone wants
to adapt it?
<p>
     <table>

     <tr>
     <td valign=top>
				<b>Contact:</b>  
     </td>
     <td valign=top>
        <A HREF="mailto:erik@NO__SPAMbinary.no">Erik Engheim</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
			<b>	Homepage:  </b>
     </td>
     <td valign=top>
        <A HREF="http://civrnf.binary.no/index.html">civrnf.binary.no</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Screenshots:</b>
     </td>
     <td valign=top>
        <A HREF="http://civrnf.binary.no/screen.html">civrnf.binary.no/screen.html</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Proposed:</b>
     </td>
     <td valign=top>
          22th September 2000
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          Looking for people to join the project <br>
          <br>
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>


  &nbsp;<p>
      <!-- *********************************************************** -->
      <h2>Proposals</h2>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <b>
    <FONT size="+1">
     <IMG SRC="images/newgame2.png" WIDTH=52 HEIGHT=27 BORDER=0 ALT="new game" ALIGN="left">
     &nbsp;
    Videogamesconsole
    </FONT>
    </b>
    <p>
The idea is to create a videogamesconsole within KDE.
The videogamesconsole within KDE must be a sort of standard for writing
videogames within KDE.
Why you want to do this, you may ask?
Well to guarantee a certain speed and gamequality at a certain minimum
specification of the PC (30fps if the PC is a pentium 166mhz, 32MB and
running a certain amount of KDE programs).
The point is with normal PC-games is that you cannot thrust their
specifications or you have to tweak speed settings or you have to
optimize your videocard or even buy a new one. 
<p>
This thing is not a sort of directX idea. More like the Mame Emulator.
In that it is not an emulator but the application run natively on your
PC's hardware benefitting from the build in power.
Developers don't have to tweak their games for a broad scope of
PC-hardware anymore. Just for one platform which is going to perform up
to the standards if the hardware meets the minimum requirements. Saving
lots of development time.
<p>
     <table>

     <tr>
     <td valign=top>
				<b>Contact:</b>  
     </td>
     <td valign=top>
        <A HREF="mailto:hansa@NO__SPAMeuronet.nl">Hans Arentsen</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
			<b>	Homepage:  </b>
     </td>
     <td valign=top>
        Not yet available
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Screenshots:</b>
     </td>
     <td valign=top>
        Not yet available
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Proposed:</b>
     </td>
     <td valign=top>
          22th September 2000
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          Looking for people to join the project <br>
          Collecting ideas<br>
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

    </td></tr></table>

<?php
INCLUDE "nospam.inc";
?>
		  
&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>
