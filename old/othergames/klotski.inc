		<!--  NEW SECTION -->
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0"
 CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0"
 BGCOLOR="#f0f0ff" ><tr><td>

    <h3>Klotski</h3>

    <table> <tr> <td>

     <IMG SRC="./images/klotski.jpg" ALT="Klotski" BORDER=0 ALIGN="right" WIDTH="104" HEIGHT="140">
     Klotski is a small brick game for Qt and has its roots in a wooden game
     named (in french) &quot;Ane rouge&quot;.<BR>
     The goal is to bring the red piece to its destination by moving other
     pieces. It is not as eaysy as it sounds!<BR>
     Klotski features about 25 levels and is fully playable.<BR>
     The game is written in Python using the
     <A HREF="http://www.thekompany.com/projects/pykde/">PyQt</A>, bindings.<BR>

    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A HREF="http://phil.freehackers.org/klotski/">phil.freehackers.org/klotski/</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
        <A HREF="http://phil.freehackers.org/klotski/">phil.freehackers.org/klotski/</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
         <a href="mailto:pfremy@NO__SPAMchez.com">Philippe Fremy</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General
         Public License (GPL).
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>



