		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0"
 CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0"
 BGCOLOR="#f0f0ff" ><tr><td>
    
    <h3>Kmamu</h3>
    
    <table> <tr> <td>
    <img src="images/mamu.gif" width="102" height="100" align="right" boeder="0">
    Kmamu is a shell script which will install icons for xmame (Multi Arcade
 Machine Emulator)
    into a folder on your KDE1 or KDE2 desktop.
    Each icon will have the long description name as provided by <tt>xmame
 -listgames</tt>,
    and when clicked it will launch xmame with the appropriate game.
    Obviously, in order to run this script you will need <a
 href="http://x.mame.net">xmame</a>.
    The icons were originally created by <a
 href="mailto:mamu@NO__SPAMsite-elite.com">MAMu_</a>,
    for <a href="http://www.classicgaming.com/mame32">MAME32</a>,
    for more information on MAME32 icons please visit <a
 href="http://www.mameicons.com/">http://www.mameicons.com/</a>.
    <p>
    Although this is not strictly a KDE game it will come in handy for the KDE
 and Mame fans.
    <br>
    (25/10/2000/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A HREF="http://tkmame.retrogames.com/kmamu/">http://tkmame.retrogames.com/kmamu/</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         not available
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:>
     </td>
     <td valign=top>
         <a href="mailto:tkmame@NO__SPAMretrogames.com">Christopher Stone</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License
 (GPL).
     </td>
     </tr>
     </table>
     
      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

