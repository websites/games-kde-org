		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h3> kPatience </h3>

    <table> <tr> <td>
    <a href="images/pat1.jpg">
    <IMG SRC="images/pat2.jpg" ALT="kPatience" BORDER="0" ALIGN="right" WIDTH="116" HEIGHT="83">
    </a>
    kPatience is a collection of well known patience games. Among them the well known
    Klondike and Freecell but also many more which are not so well known. The game
    has extremely nice graphics and is very comfortable to play. A hint and demo
    function is just as well included as the possibility to select a card deck of
    your choice.
    <p>
    I think it is best summed up with the words of a friend of mine who is used to
    Windows(tm) and tried the kPatience: <em>Wow, that game is super cool and
    way better than the Windows patience I am used to play. Everybody should play that.</em>.
    <p>
    This game is a must if you ask me!
    <br>
    (15/03/2001/<a href="mailto:martin@NO__SPAMheni-online.de">MH</a>)
    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A HREF="http://games.kde.org/status.html">http://games.kde.org/status.html</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         <A HREF="images/pat1.jpg">imagespat1.jpg</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
         <a href="mailto:coolo@NO__SPAMkde.org">Stephan Kulow</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License (GPL).
     </td>
     </tr>
     </table>
     
      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>
		  
