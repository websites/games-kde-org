		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0"
 CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0"
 BGCOLOR="#f0f0ff" ><tr><td>
    
    <h3>Four Wins</h3>
    
    <table> <tr> <td>
     <IMG SRC="images/kwin4.png" ALT="Four wins" WIDTH="88" HEIGHT="70" BORDER=0 ALIGN="right">
     This is a nice two player boardgame four Linux/KDE2. It follows the rules
 of the
     boardgame Connect Four (tm).
     Each player is represented by a color (yellow and red). The goal of the
 game is to get
     four connected pieces of your color into a row, column or any diagonal.
 This is done by
     placing one of your pieces into any of the seven columns. A piece will
 begin to fill a
     column from the bottom, i.e. it will fall down until it reaches the ground
 level or
     another stone. After a move is done it is the turn of the other player.
 This is repeated
     until the game is over, i.e. one of the players has four pieces in a row,
 column or
     diagonal or no more moves are possible because the board is filled.
    <p>
    </td>
    </tr></table>

     <table>

     <tr>
     <td valign=top>
				Homepage:
     </td>
     <td valign=top>
        <A
 HREF="http://www.heni-online.de/linux/linux.html#kwin4">www.heni-online.de/linu
x/linux.html#kwin4</A>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Screenshots:
     </td>
     <td valign=top>
         <A
 HREF="http://www.heni-online.de/linux/linux.html#kwin4">www.heni-online.de/linu
x/linux.html#kwin4</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        Contact:
     </td>
     <td valign=top>
         <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        License:
     </td>
     <td valign=top>
        Code and Graphics are published under the GNU General Public License
 (GPL).
     </td>
     </tr>
     </table>


      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

