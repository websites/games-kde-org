<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KPuzzle </h3>

  <table><tr><td>

    <p>
    <a href="images/kpuzzle1.png">
    <img src="images/kpuzzle2.png" ALT="KPuzzle" BORDER="0" ALIGN="right" WIDTH="116" HEIGHT="102">
    </a>
KPuzzle is a jigsaw puzzle game for KDE 2 featuring different
game types with varying difficulties. It should run everywhere
where KDE runs, too.</p>

<p> Despite the low version number, the game is sufficiently stable
to be entirely playable. 
What KPuzzle mainly lacks before really being finished is a
lot of testing on different systems with different configurations
and feedback from its users telling me what could be improved,
especially in the game control.</p>

<p>These are only some of KPuzzle's features:
<ul>
      <li>Support of various game types with different
	difficulty</li>
      <li>Multiple picture sets (you can, of course, use
	every picture you want)</li>
      <li>Full saving/loading support</li>
      <li>Internationalization support (note that only
	the GUI has been translated to German until now)</li>
</ul>
</p>
<p>The <a href="http://kpuzzle.sourceforge.net/download.html">
      download page</a> contains various download links to
    the game itself and to picture sets accompanying it.</p>
<p>There is a mailing list where you may ask any questions
    about KPuzzle. Find information on it at
    <a href="http://lists.sourceforge.net/lists/listinfo/kpuzzle-users">
      The List's Homepage</a>.</p>
(27/05/2001/<a href="mailto:mwand@NO__SPAMgmx.de">MW</a>)

   </td>
  </tr></table>
  <table>
    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
    
       <a href="http://kpuzzle.sourceforge.net/">http://kpuzzle.sourceforge.net/</a>

    </td></tr>
    <tr><td valign=top>
       Screenshots:
    </td><td valign=top>
    
       <a href="http://kpuzzle.sourceforge.net/screenshot.html">kpuzzle.sourceforge.net/screenshot.html</a>

    </td></tr>
    <tr><td valign=top>
       Contact:
    </td><td valign=top>
    
       <a href="mailto:mwand@NO__SPAMgmx.de">Michael Wand</a>

    </td></tr>
    <tr><td valign=top>
      License:
    </td><td valign=top>
    
      Code and Graphics are published under the GNU General Public License (GPL).
      
    </td></tr>
  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

