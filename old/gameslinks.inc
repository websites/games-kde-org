<?php 
  $piximage="<img src=\"/img/clear-dot.gif\" width=\"15\" height=\"1\" alt=\" \" border=\"0\" />";
?>


    &nbsp;<br>
    <?php 
      $text="The KDE Games Center";
      if ($myfile=="index")
      {
        echo ("<b><font color=\"#FF0000\">$text</font></b>\n");
      }
      else
      {
        echo ("<b><a href=\"/kdegames/index.html\">$text</a></b>\n");
      }
    ?>
    &nbsp;<br>
    &nbsp;<br>

    <b><font color="#0000EE">KDE Games package</font></b>
    <br>

    <!-- Now the entries for the KDE games package -->
    <!-- Arcade -->
    <?php echo $piximage; ?>
    <?php 
      $text="Arcade Games";
      if ($myfile=="arcade")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/kde_arcade.html\">$text</a>\n");
      }
    ?>
    <br>

    <!-- Boardgames -->
    <?php echo $piximage; ?>
    <?php 
      $text="Board Games";
      if ($myfile=="boardgames")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/kde_boardgames.html\">$text</a>\n");
      }
    ?>
    <br>

    <!-- Cardgames -->
    <?php echo $piximage; ?>
    <?php 
      $text="Card Games";
      if ($myfile=="cardgames")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/kde_cardgames.html\">$text</a>\n");
      }
    ?>
    <br>

    <!-- Tactic&Strategy -->
    <?php echo $piximage; ?>
    <?php 
      $text="Tactic&amp;Strategy";
      if ($myfile=="tacticgames")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/kde_tactics.html\">$text</a>\n");
      }
    ?>
    <br>

    &nbsp;<br>

    <!-- Now a list of all games and their home pages if available -->
    <?php echo $piximage; ?> <b><font color="#0000cc">Arcade:</font></b><br>
    <?php echo $piximage; echo $piximage; ?> kAsteroids <br>
    <?php echo $piximage; echo $piximage; ?> kBounce <br>
    <?php echo $piximage; echo $piximage; ?> kSnake <br>
    <?php echo $piximage; echo $piximage; ?> kSmiletris <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://ksirtet.sourceforge.net">kSirtet</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://www.azweb.de/kspaceduel/">kSpaceduel</a> <br>
    <?php echo $piximage; echo $piximage; ?> kTron <br>

    <?php echo $piximage; ?> <b><font color="#0000cc">Boardgames:</font></b><br>

    <?php echo $piximage; echo $piximage; ?> kBackgammon <br>
    <?php echo $piximage; echo $piximage; ?> kBlackbox <br>
    <?php echo $piximage; echo $piximage; ?> kEnolaba <br>
    <?php echo $piximage; echo $piximage; ?> <a href="kmahjongg/index.html">kMahjongg</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://kmines.sourceforge.net">kMines</a> <br>
    <?php echo $piximage; echo $piximage; ?> kReversi <br>
    <?php echo $piximage; echo $piximage; ?> kShisen <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://hem.passagen.se/awl/">kSokoban</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://www.heni-online.de/linux/index.html#kwin4">kWin4</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="/kdegames/kbattleship/index.html"> KBattleship </a><br>


    <?php echo $piximage; ?> <b><font color="#0000cc">Cardgames:</font></b><br>

    <?php echo $piximage; echo $piximage; ?> kPat <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://kpoker.sourceforge.net">kPoker</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://www.heni-online.de/linux/index.html#lskat">lsKat</a> <br>

    <?php echo $piximage; ?> <b><font color="#0000cc">Tactic&amp;Strategy:</font></b><br>

    <?php echo $piximage; echo $piximage; ?> kJumpingcube <br>
    <?php echo $piximage; echo $piximage; ?> Konquest <br>
    <?php echo $piximage; echo $piximage; ?> kSame <br>

    <?php echo $piximage; ?> <b><font color="#0000cc">Kids games:</font></b><br>
    <?php echo $piximage; echo $piximage; ?> <a href="http://www.bureau-cornavin.com/opensource/ktuberling/index.html">kTuberling</a> <br>

    <p>



    &nbsp;<br>
    <b><font color="#0000EE">Other KDE Games</font></b>
    <br>

    <?php echo $piximage; ?>
    <?php 
      $text="Editor's Choice";
      if ($myfile=="othergames")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/othergames.html\">$text</a>\n");
      }
    ?>
    <br>

    <?php echo $piximage; ?>
    <?php 
      $text="Top 10 KDE Games";
      if ($myfile=="top10")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/top10.html\">$text</a>\n");
      }
    ?>
    <br>

    <?php echo $piximage; ?>
    <a href="http://kde-apps.org">kde-apps.org</a>
      &nbsp;<?php echo ("<img src=\"images/away3.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    <p>

    &nbsp;<br>
    <b><font color="#0000EE">Programming</font></b>
    <br>


    <?php echo $piximage; ?>
    <?php 
      $text="Articles&amp;Docs";
      if ($myfile=="gamedoc")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/gamedoc.html\">$text</a>\n");
      }
    ?>
    <br>

    <?php echo $piximage; ?>
    <?php 
      $text="Tools&amp;Utilities";
      if ($myfile=="tools")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/tools.html\">$text</a>\n");
      }
    ?>
    <br>


    &nbsp;<br>
    <b><font color="#0000EE">Projects</font></b>
    <br>

    <?php echo $piximage; ?>
    <?php 
      $text="New Games";
      if ($myfile=="newgames")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/newgames.html\">$text</a>\n");
      }
    ?>
    <br>

    <?php echo $piximage; ?>
    <?php 
      $text="People";
      if ($myfile=="people")
      {
        echo ("<font color=\"#FF0000\">$text</font>\n");
      }
      else
      {
        echo ("<a href=\"/kdegames/people.html\">$text</a>\n");
      }
    ?>
    <br>
    <?php echo $piximage; echo $piximage; ?> <a href="/kdegames/people.html#graphics">Graphics</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="/kdegames/people.html#sound">Sound</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="/kdegames/people.html#programming">Programming</a> <br>
    <?php echo $piximage; echo $piximage; ?> <a href="/kdegames/people.html#misc">Miscellaneous</a> <br>
    <p>

    <p>



