<?php
  $page_title = "Tools&Utilities";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>
		  
		  <FONT SIZE="+1">T</FONT>his section contains information
      about tools and utilities helpful for games development.
      <p>
      <FONT SIZE="+1">I</FONT>f you know of any nice tool which should be presented here
      <a href="mailto:martin@NO__SPAMheni-online.de">drop us a note</a>.

      <p>&nbsp;<p>

    <TABLE align="center" border="0" width="90%"><TR><TD>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h2>KDE Gnu Gaming Zone</h2>
       A library to allow KDE games to be played over the internet and
       people to meet and discuss in chats. It is
       comparable to the battle.net gaming zones.
       <p>
       There exists also a 
       <a href="ggz-kde-preview.html">detailed description</a> of the library.
       <p>
       The library is currently under development. It is intended to combine it
       with the KDE <em>Network game programming library</em> (see below).
       <p>
     <table>

     <tr>
     <td valign=top>
			<b>	Contact:  </b>
     </td>
     <td valign=top>
        <a href="mailto:dr_maux@NO__SPAMmaux.de">Josef Spillner </a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Type:</b>
     </td>
     <td valign=top>
         Network game programming library
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          pre-alpha
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h2>KDE Games Network Library</h2>
       Fully object oriented network game library. The library allows
       KDE games very easy to use and transparent network connections.
       Other features include a property system, easy to use load/save
       functions as well as support for computer players.
       <p>
       The library offers an object structure for games on the basis of
       <em>Game</em>, <em>Player</em> and <em>IODevices</em> objects
       which you can connect to each other and which will automatically
       interact then.
       <p>
       Additional features are the support of computer players,
       either as separate processes or intergrated in the main program
       just as fully transparent and easy to use load/save handling.
       <p>
       You can find and read some
       <a href="http://www.heni-online.de/libkdegames/index.html">documentation</a>
       and the library is included in KDE 3.x
       <p>
     <table>

     <tr>
     <td valign=top>
			<b>	Contact:  </b>
     </td>
     <td valign=top>
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> <br>
        <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann </a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Type:</b>
     </td>
     <td valign=top>
         Network game programming library
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          stable
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h2>Simple DirectMedia Layer</h2>
    
        Simple DirectMedia Layer is a cross-platform multimedia
        library designed to provide fast access to the graphics
        framebuffer and audio device.
        <br>
        It is used by MPEG playback
        software, emulators, and many popular games, including the
        award winning Linux port of "Civilization: Call To Power."
        Simple DirectMedia Layer supports Linux, Win32, BeOS,
        MacOS, Solaris, IRIX, and FreeBSD.
      <p>

      <b>What can it do?</b><p>
      <ul>
      <li> direct and accellerated video access</li>
      <li> event handling (keyboard,mouse,...)</li>
      <li> threaded audio handling</li>
      <li> CD-Rom audio</li>
      <li> Threads</li>
      <li> Timers</li>
      <li> Endian independence</li>
      </ul>
      <p>

     <table>

     <tr>
     <td valign=top>
			<b>	Homepage:  </b>
     </td>
     <td valign=top>
        <A HREF="http://www.libsdl.org/">http://www.libsdl.org/</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Type:</b>
     </td>
     <td valign=top>
         Game programming library
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          stable
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>

		<!--  NEW SECTION -->  
    <TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
		<table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
    <h2>Ksenomorph and Ksenobite</h2>
    
      The goal of Ksenomorph project is to create a small and handy editor of 3D objects that is
      suitable for using in OpenGL(tm) programs. Ksenomorph output files can easily be loaded in
      memory from your programs because it's format is very simple and looks like an OpenGL(tm)
      program. 
      <p>
      The goal of Ksenobite project is to create a program for creating an animated 3D models from
      objects builded by Ksenomorph. Today this project is on early development stage. 

      <p>

     <table>

     <tr>
     <td valign=top>
				<b>Contact:</b>  
     </td>
     <td valign=top>
        <A HREF="mailto:sleepws@NO__SPAMyahoo.com">Dmitry Samoyloff </A>
         
     </td>
     </tr>

     <tr>
     <td valign=top>
			<b>	Homepage:  </b>
     </td>
     <td valign=top>
        <A HREF="http://www.geocities.com/sleepws/eng/projects.html">www.geocities.com/sleepws</a>
     </td>
     </tr>

     <tr>
     <td valign=top>
        <b>Type:</b>
     </td>
     <td valign=top>
         3D (OpenGL) data generation tool 
     </td>
     </tr>
     <tr>
     <td valign=top>
        <b>Status:</b>
     </td>
     <td valign=top>
          Early development stage
     </td>
     </tr>
     </table>

      </td></tr></table>
      </td></tr></table>
      &nbsp;<br>



      </TD></TR></TABLE>

<?php
INCLUDE "nospam.inc";
?>
		  
&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>
