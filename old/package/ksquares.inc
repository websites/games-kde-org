<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSquares </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ksquares.png" ALT="KSquares" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="117">
        <em>KSquares</em> is an implementation of the popular paper
        based game squares. You must draw lines to complete squares,
        the player with the most squares wins. You can play with up
        to 4 players, any number of which may be controlled by the
        computer.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:matt@NO__SPAMmilliams.com">Matt Williams</a>
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a
        href="http://milliams.com/content/view/18/42/">KSquares at milliams.com</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.3 - KDE 4.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-4 player <br> 
        <img src="images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>
