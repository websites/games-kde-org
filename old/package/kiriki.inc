<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> Kikiri </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kiriki.png" ALT="Kiriki" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="100">
      <em>Kiriki</em> is the KDE version of the dice game <em>Yahtzee</em>
      where you roll dices to get higher scores in several combinations.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:aacid@kde.org">Albert Astals Cid</a> 
    </td></tr>

    <tr><td valign=top>
           Homepage:
    </td><td valign=top>
        <a href="http://home.gna.org/kiriki/">http://home.gna.org/kiriki/</a>
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-6 players <br> 
               <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

