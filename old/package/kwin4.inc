<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> kWin4 </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kwin4b.png" ALT="kWin4" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="89">
      <me>kWin4</em> or <em>Four wins</em> is a two player board game which follows
      the rules for the <em>Connect Four (TM)</em> board game where
      you have to align four pieces of the same colour to win.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a
        href="http://www.heni-online.de/linux/index.html#kwin4">www.heni-online.de/linux</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

