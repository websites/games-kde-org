<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KAsteroids </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kasteroids.png" ALT="KAsteroids" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="98">
     <em>KAsteroids</em> is a fast arcade shooting game where the
     player has to command a spaceship and survive the passage
     of an asteroid field. This is done by shooting at
     the asteroids which then split into smaller parts until
     nothing is left.
     <br>
     The game has fast and smooth graphics and is easy to learn
     and play.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Author:
    </td><td valign=top>
        <a href="mailto:mjones@NO__SPAMkde.org">Martin R. Jones</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.2 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

