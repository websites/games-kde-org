<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KBattleship </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kbattleship.png" ALT="kBattleship" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="84">
        <em>KBatteship</em> is a KDE implentation of the
        popular game "Battleship" where you have to try to sink
        the opponents ships. The game can also be played with friends
        online via the internet.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:wildfox@NO__SPAMkde.org">Nikolas Zimmermann</a><br>
        <a href="mailto:molkentin@NO__SPAMkde.org">Daniel Molkentin</a>
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a
        href="/kdegames/kbattleship/index.html">games.kde.org/kdegames/kbattleship</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

