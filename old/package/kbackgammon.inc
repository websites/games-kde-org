<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KBackgammon </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kbackgammon.png" ALT="kBackgammon" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="113">
          This is a graphical backgammon program following the rules
          of the boardgame <em>Backgammon</em>. The KDE version supports 
          backgammon games with other players, games against
          computer engines like <em>GNU bg</em> and even
          online games.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:jens@NO__SPAMhoefkens.com">Jens Hoefkens</a><br>
        <a href="mailto:gobo@NO__SPAMimada.sdu.dk">Bo Thorsen</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.5.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

