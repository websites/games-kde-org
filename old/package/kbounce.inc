<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KBounce </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kbounce.png" ALT="KBounce" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="87">
        Your task in <me>KBounce</em> (<em>Jezz Ball</em>) is to catch 
        several moving balls in a rectangular game field by building walls.
        The motivation consists of finding new and advanced strategies to 
        catch as many balls as possible.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:1Stein@NO__SPAMgmx.de">Stefan Schimanski</a><br>
        <a href="mailto:ssigala@NO__SPAMglobalnet.it">Sandro Sigala</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.5 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

