<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KMines </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kmines.png" ALT="KMines" BORDER="0" ALIGN="right" WIDTH="97" HEIGHT="120">
          <em>KMines</em> is  the classical
          <em>Minesweeper</em> game where you
          have to find mines by logical deduction.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:hadacek@NO__SPAMkde.org">Nicolas Hadacek</a><br> 
        Andreas Zehender
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.1.4 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

