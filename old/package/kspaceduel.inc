<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSpaceDuel </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kspaceduel.png" ALT="KSpaceDuel" BORDER="0" ALIGN="right" WIDTH="119" HEIGHT="93">
      <em>KSpaceduel</em> is a space arcade game for two players. However, one player can be
      controlled by the computer.
      Each player controls a satellite that flies around the sun. While doing
      so both players try not to collide with anything but shoot at the other
      space ship.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:az@NO__SPAMazweb.de">Andreas Zehender</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a href="http://www.azweb.de/kspaceduel/">www.azweb.de/kspaceduel</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

