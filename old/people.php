<?php
  $page_title = "KDE Games People";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

		  <FONT SIZE="+1">H</FONT>ere people who can and want to contribute to
      various KDE game projects are collected. This overview
      on who wants and can do what job is meant to help in the coordination
      of new KDE games. Furthermore, it helps finding the right addition
      to your game team. Say you lack a sound effect composer - here you'll
      find it.
      <p>
      <FONT SIZE="+1">I</FONT>f you need someone to help you with graphics or sound effects in your
      project simply click on a name in the lists and write them an email!
      <br>
      <FONT SIZE="+1">I</FONT>f your project suceeds it would be nice having a bit feedback for
      this page so that we can link to successful projects or demo sounds
      and graphics!
      <p>
      &nbsp;
      <p>
      <FONT SIZE="+1">I</FONT>f you yourself want to be part of the KDE games developer team
      drop a mail to
      <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> or
       <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann</a> and we will include
       you on this page.
  <p>

  &nbsp;<p>

    <TABLE align="center" border="0" width="90%"><TR><TD>
		<!--  NEW SECTION -->  
		  

      <h2>
      <a NAME="graphics">
      Graphics and artists
      </a>
      </h2>
  
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th width="15%" >Name</th>
        <th width="20%">Interests</th>
        <th width="25%">Fame to claim</th>
        <th width="40%">Comments</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:benadler@NO__SPAMgmx.net">Benjamin Adler</a>
      </td>
      <td>
         2D graphics
      </td>
      <td>
        <em>Boson</em> graphics
      </td>
      <td >
        Did <em>Boson</em> game themes.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:bastian.salmela@NO__SPAMhel.fi">Bastian Salmela</a>
      </td>
      <td>
         2D graphics<br>
         some 3D objects
      </td>
      <td>
        Penguin theme of <em>Knights</em>
      </td>
      <td >
      Did 2D computer graphics with
      Koala painter on the C 64, Deluxe Paint on a PC,
      PaintShopPro on Windows, Gimp on Linux and 3D
      graphics with 3d Studio Max and Blender. 
      Example can be found on 
      <a href="http://www.lib.hel.fi/~basse/">http://www.lib.hel.fi/~basse/</a>
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:Elmar.Hoefner@NO__SPAMuibk.ac.at">Elmar Hoefner</a>
      </td>
      <td>
         2D graphics
      </td>
      <td>
        kBattelship graphics
      </td>
      <td >
      Started computer graphics with manipulating photos 
      using Corel's PhotoPaint. Now the focus is on designing icon-like pictures
      and background arts.
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      <p>
      &nbsp;
      <p>

    </td></tr>
		<!--  NEW SECTION -->  
    <tr><td>

      <h2>
      <a NAME="sound">
      Music and Sound
      </a>
      </h2>
  
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th width="15%">Name</th>
        <th width="20%">Interests</th>
        <th width="25%">Fame to claim</th>
        <th width="40%">Comments</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:todd@NO__SPAMslideburn.com">Todd Chapman </a>
      </td>
      <td>
         Sound effects<br>
         Game music
      </td>
      <td>
        Full time musician
      </td>
      <td >
          To contribute music and some sound effects/themes.  Being
          "foley artist" for a day and just bang on some pots-n-pans and other
          household artifacts for obscure sounds to be used for a library.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:projectgroove@NO__SPAMgmx.net">Manuel Fischer</a>
      </td>
      <td>
         Sound effects<br>
         Jingles<br>
         electronic game music
      </td>
      <td>
        <a href="http://www.quake.de">Party soundtracks</a>, 
        <a href="http://www.virtual-volume.com/artist/cyberjunkie">Various tracks</a> and
        KDE-startsound
      </td>
      <td >
      Made Lan-party sound track called <em>The first contact</em>
      on <a href="http://www.project-groove.de">http://www.project-groove.de</a>.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:ageberovich@NO__SPAMgmx.at">Alejandro Geberovich </a>
      </td>
      <td>
         Composing<br>
         Game music
      </td>
      <td>
        Pianist at the Konservatorium in Vienna and
        composer
      </td>
      <td >
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:ludovic.grossard@NO__SPAMlibertysurf.fr">Ludovic Grossard</a>
      </td>
      <td>
         Game music<br>
         Jingles
      </td>
      <td>
        Music composer and some
        <em>Boson</em> game music
      </td>
      <td >
        Documentation coordinator of the KDE french team.
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:Nils.Trzebin@NO__SPAMstud.uni-hannover.de">Nils Trzebin</a>
      </td>
      <td>
         Game music<br>
         Sound effects
      </td>
      <td>
        <em>Boson</em> game music
      </td>
      <td >
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:Kasper.Souren@NO__SPAMircam.fr">Kasper Souren</a>
      </td>
      <td>
         Game music<br>
         Sound effects
      </td>
      <td>
      Composer, laptop musician, music researcher
      </td>
      <td >
      Mainly works with Cheesetracker. See 
      <a href="http://www.industree.org">here</a> and also
      <a href="http://www.mp3.com/industree">this for some of his music</a>.
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      <p>
      &nbsp;
      <p>

    </td></tr>
		<!--  NEW SECTION -->  
    <tr><td>

      <h2>
      <a NAME="programming">
      Programming
      </a>
      </h2>
  
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th width="15%">Name</th>
        <th width="20%">Interests</th>
        <th width="25%">Fame to claim</th>
        <th width="40%">Comments</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:b_mann@NO__SPAMgmx.de">Andreas Beckermann </a>
      </td>
      <td>
         Card games<br>
         Network library
      </td>
      <td>
         kPoker and libkdegames
      </td>
      <td >
        Develops the KDE network game library  
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:orzel@NO__SPAMkde.org">Thomas Capricelli </a>
      </td>
      <td>
         Strategy games
      </td>
      <td>
         Boson
      </td>
      <td >
      </td>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a>
      </td>
      <td>
         Card games<br>
         Network library
      </td>
      <td>
         Lieutnant Skat and Four Wins
      </td>
      <td >
        Develops the KDE network game library  
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      <p>
      &nbsp;
      <p>

    </td></tr>
		<!--  NEW SECTION -->  
    <tr><td>

      <h2>
      <a NAME="misc">
      Miscellaneous/Beta Testers
      </a>
      </h2>
  
      <table border="1" bgcolor="#F0F0FF">
      <tr align="left">
        <th width="15%">Name</th>
        <th width="20%">Interests</th>
        <th width="25%">Fame to claim</th>
        <th width="40%">Comments</th>
      </tr>
      <!-- ******************************************************* -->
      <tr align="left" >
      <td >
        &nbsp;
      </td>
      <td>
         &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td >
        &nbsp;
      </td>
      </tr>
      <!-- ******************************************************* -->
      </table>

      <p>
      &nbsp;
      <p>

    </td></tr>
		<!--  END TABLE -->  
    </table>



<?php
INCLUDE "nospam.inc";
?>
&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>
