<?php
  $page_title = "Top 10 KDE Games";
  include "games.inc";

  $author="Rob Kaper";
  $mail="kaper@NOSPAM.kde.org";
  include "header.inc";

  // TODO: does kde-apps.org have a feed we could use to put back the list
  // here? (see the top10rdf.raw file)
?>

<p>
For a top 10 list of KDE games, see the <a
href="http://www.kde-apps.org/index.php?xcontentmode=250x251x252x253x254&amp;xsortmode=high&amp;page=0">highest
rated games on KDE-Apps.org</a>. Many games (and other applications) are
listed on KDE-Apps.org.</p>

<p>To keep the top list up-to-date it would be very helpful if you could rate some games there!</p>

<?php
INCLUDE "nospam.inc";
?>

&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>
