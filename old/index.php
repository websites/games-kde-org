<?php
  $page_title = "The KDE (Legacy) Games Center";
  include "games.inc";
  $site_menus = 2;

  $author="M. Heni";
  $mail="linux@NO__SPAMheni-online.de";
  include "header.inc";
  
?>
      <h2>Content</h2>
      <p>
         <b>
            This Website and it contents are old.Newer version of these games could be found in <a href='..'>our new website which is dedicated to KDE 4.x Games.</a>
         </b>
      </p>
      <p>
      <font size="+1">T</font>he <b>KDE Games Center</b>
      contains information on
      games and games developing under KDE 3.x.
      </p>
      <p>
      <font size="+1">I</font>t consists of sections on the games delivered with
      the current KDE release as well as a collection of projects, new games or
      games in planning.
      </p>

      <p>
      <font size="+1">D</font>evelopers also find here some useful links and tips on how 
      to write games in KDE.
      </p>

      <h2>Responsibilities</h2>

      <p>
      <font size="+1">P</font>eople maintaining this site, the games
      package and other important things concerning game development.
      </p>

      <dl>
      <dt><b>Package Maintainer:</b></dt>
      <dd>
        <!-- Matthias ask me to be removed for the time after KDE 2.0
        Matthias Hoelzer-Kluepfel &nbsp;&nbsp; (<a href="mailto:mhk@NO__SPAMcaldera.de">mhk@NO__SPAMcaldera.de</a>)
        -->
        <!-- MH: Discussed on the kde-games-devel mailing list April 2001 
        -->
        Andreas Beckermann  &nbsp;&nbsp;(<a href="mailto:b_mann@NO__SPAMgmx.de">b_mann@gmx.de</a>)
      </dd>

      <dt><b>Translation and Documentation:</b></dt>
      <dd>
         <a href="http://i18n.kde.org/">http://i18n.kde.org/</a>
      </dd>

      <dt><b>Mailing List:</b></dt>
      <dd>
         <a
         href="http://mail.kde.org/pipermail/kde-games-devel/">kde-games-devel</a>
         at <br />
         <a href="http://mail.kde.org/mailman/listinfo/kde-games-devel/">http://mail.kde.org/mailman/listinfo/kde-games-devel/</a>
      </dd>

      <dt><b>Webpage:</b></dt>
      <dd>
        Martin Heni  &nbsp;&nbsp;(<a href="mailto:linux@NO__SPAMheni-online.de">linux@heni-online.de</a>) <br />
        Andreas Beckermann  &nbsp;&nbsp;(<a href="mailto:b_mann@NO__SPAMgmx.de">b_mann@gmx.de</a>)
      </dd>
      </dl>


      <!-- News -->
      <h2>Game News</h2>
      <?php
      $news=file("news.raw");
      $cnt=count($news);
      echo ("<table>\n");
      for ($i=0;$i<$cnt;$i++)
      {
        $items=explode(":",$news[$i],2);
        if (count($items)>1)
        {
          echo ("<tr valign=\"top\"><td nowrap=\"nowrap\" align=\"right\"><b>".$items[0]."</b></td>\n");
          echo ("<td nowrap=\"nowrap\">&nbsp;&nbsp;</td>\n");
          echo ("<td>".$items[1]."<br /></td></tr>\n");
        }
        else
        {
          echo ("<tr valign=\"top\"><td nowrap=\"nowrap\" align=\"right\">&nbsp;</td>\n");
          echo ("<td nowrap=\"nowrap\">&nbsp;&nbsp;</td>\n");
          echo ("<td>".$news[$i]."<br /></td></tr>\n");
        }
      }
      echo ("</table>\n");
      ?>



      <h2><font color="#FF0000">WANTED</font></h2>

      <p>
      <font size="+1">F</font>or making this site super cool and useful for all
      KDE game programmers we always need further information and links like:
      </p>
      <ul>
      <li>Tips and tricks for game developing in KDE and general game development</li>
      <li>Links to such pages in the internet, like gamedev.net but many more.</li>
      <li>Information about your games for KDE</li>
      <li>Release or updates of your games if you want them to appear in the news section</li>
      </ul>
      <p>
      Please send all information, ideas and critics to 
      <a href="mailto:linux@NO__SPAMheni-online.de">linux@heni-online.de</a>.
      </p>

<hr width="570" size="5" align="left" noshade="noshade" />
<font size="-1">
          Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>

      
