---
layout: post
date: 2008-01-11
title: Today is THE day!
category: news
---

Ladies and Gentlemen,
**KDE4 v 4.0** has been released!

And that means – KDEGames v 4.0 has been released along! The whole team is overjoyed, and so I'm sure, are you. It was a difficult year. Year full of a thousand cares and worries. Year of hard, fast paced, nonstop development. But it's all over now.

KDEGames team hopes you will enjoy playing our games just as much as we enjoyed making them. And do trust me, we enjoyed it a lot.