---
layout: post
date: 2008-01-04
title: KDE4 has been tagged.
category: news
---

Ladies and Gentlemen,
KDE4 has been officially tagged for 4.0 release on January 11th. This means our winter vacations are indeed over, and back to work we go!

Form now on the intensive development of KDEGames begins anew; which in turn means bringing in a lot of new, exciting features and improving the existing ones.

Wish us luck dear games, and don't forget to write :D