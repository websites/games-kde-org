---
layout: post
date: 2008-03-03
title: A great addition to KDEGames family.
category: news
---

It is our pleasure to welcome a new game into our selection. **Kollision** - a wonderful, addictive, and in every way innovative game straight from the lab of our gaming guru Paolo Capriotti. While it may seem very simple at the first glance, **Kollision** is one of the harder games in the whole gamedom, and what's best - it's all about high score. And all you have to do really, is to stay game for as long as possible.

<img class="news-img text-center" src="/pics/kollision01.png">

In **Kollision** you use the mouse to control a small blue ball in a closed space environment filled with small red balls, which move about chaotically. Your goal is to avoid touching any of those red balls with your blue one, because the moment you do the game will be over. The longer you can stay in game the higher your score will be.

The game is now in the KDEGames 4 package and will make its debut as soon as KDE 4.1 is released. However, the impatient ones can check it out now by compiling KDEGames from source (Click here to see how).

Have fun playing.