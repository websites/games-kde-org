---
layout: post
date: 2007-12-20
title: The future is approaching fast!
category: news
---

Only 21 days left until KDE 4.0 goes final! This means – only just 14 days left until KDE 4.0 tagging begins. All the changes and alterations to the code and artwork have to be submitted within these 14 days! Thereafter we all can relax and have a drink or two. Best of luck to all the contributors to KDEGames and all other KDE projects. We shall be victorious!