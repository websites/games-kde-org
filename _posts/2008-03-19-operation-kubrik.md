---
layout: post
date: 2008-03-19
title: Operation Kubrik
category: news
---

**Kubrik** - the very latest addition to KDEGames package – is none other than a glamorous three dimensional (3D) **Rubik's Cube** game. The development is now nearing completion and we hope to offer the game to you as soon as KDE 4.1 release.

The original game of **Rubik's Cube** is quite famous, and is known to be the best selling toy ever made. The game is a puzzle and is usually played with a cube made out of small blocks, 9 of those per side. The sections can be spoon around horizontally as well as vertically, however not diagonally. From the start the cube is 'scrambled', meaning that each of its 6 sides is a multicolored mess. The player then has to rearrange the colors in such a manner that each of the six sides becomes one and the same color.

In essence the game of **Kubrik** simulates the original toy very well, allowing however for far more complicated puzzles than the original. The entire game play is situated in reach and visually clean 3D environment where you use a mouse to control the cube's various rotations. That as well as the ability to save and restore the game at any time will surely make **Kubrik** into a real highlight of KDEGames' impressive collection.

Using the occasion we, at KDEGames, also would like to mention that today **Kubrik**'s developer, and one of KDEGames' more prominent members - Ian Wadham is celebrating a very special jubilee. We wish Ian all the best, and many-many games ahead.

Happy Birthday Ian!