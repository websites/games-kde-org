---
layout: post
date: 2007-12-26
title: Happy Holidays!
category: news
---

KDEGames team wishes you all the best this holiday season. May these days be merry and bright for you and your loved ones.

In case you were wondering – the new, redesigned website will be released along with the rest of KDE 4.0 goodness in just 15 days. In the mean time please be patient.