---
layout: post
date: 2007-12-31
title: Happy New Year!
category: news
---

Dear Gamers,

KDEGames team wishes to thank you for playing KDEGames all through 2007. We want to continue entertaining you in the coming 2008. With that we promise to make our games better, happier, and even more engaging then they already are :D

<img class="news-img text-center" src="/pics/konqui.sticker.png">

Happy New Year dear friends, and all the best to you!

Best wishes,

KDEGames team.