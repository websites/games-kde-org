---
layout: post
date: 2007-12-10
title: The last KDEGames meeting... This year that is!
category: news
---

This coming Thursday, December the 13th (wow, great date!) KDEGames team will gather for the very last meeting in 2007, before we all go to the long deserved holiday vocations.

It is on this meeting that the road to KDE 4.1 shall be mapped, and all the issues of KDE 4.0 brought to light and discussed.

Also, our very own Emil has created a wonderful KDE4 release day counter:

<img style="height: 4rem; width: auto;" src="/new/counter/days/En/released.png">

Enjoy